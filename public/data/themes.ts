const themes = [
	'auto',
	'atlantis',
	'brutal-dark',
	'brutal-light',
	'dracula',
	'hacker',
	'joker',
	'modern-dark',
	'modern-light',
	'monokai-sweet-dark',
	'reader',
];

export {
	themes,
}